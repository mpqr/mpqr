!! ##############################################################################################
!!
!! Copyright 2012-2020 CNRS, INPT
!! Copyright 2013-2015 UPS
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


module cli_mod

  use iso_fortran_env
  

private

  integer, parameter :: long_len  = 64
  integer, parameter :: short_len = 4
  integer, parameter :: dscr_len  = 256
  integer, parameter :: max_parms = 64
  
  type :: arg_type
     private
     character(len=:), allocatable :: switch
     character(len=:), allocatable :: description
     integer                       :: position
     logical                       :: present
     logical                       :: required
  end type arg_type
  

  type, extends(arg_type) :: iarg_type
     private
     integer              :: val
     integer              :: def
     character(len=7)     :: typ='Integer'
  end type iarg_type

  type, extends(arg_type) :: rarg_type
     private
     real(real32)         :: val
     real(real32)         :: def
     character(len=4)     :: typ='Real'
  end type rarg_type

  type, extends(arg_type) :: farg_type
     private
     logical              :: val
     logical              :: def
     character(len=4)     :: typ='Flag'
  end type farg_type

  type, extends(arg_type) :: sarg_type
     private
     character(len=:), allocatable :: val
     character(len=:), allocatable :: def
     character(len=6)              :: typ='String'
  end type sarg_type

  type :: arg_list_type
     class(arg_type), allocatable :: arg
  end type arg_list_type


  type :: cli_type
     character(len=:), allocatable, private :: title
     character(len=:), allocatable, private :: description
     integer                      , private :: na              ! the number of arguments
     type(arg_list_type)          , private :: arg_list(max_parms)
   contains
     procedure, public             :: init     => cli_init
     procedure, public             :: added    => cli_added
     procedure, public             :: add_iarg => cli_add_iarg
     procedure, public             :: add_rarg => cli_add_rarg
     procedure, public             :: add_farg => cli_add_farg
     procedure, public             :: add_sarg => cli_add_sarg
     generic  , public             :: add      => add_iarg, add_rarg, add_farg, add_sarg
     procedure, public             :: help     => cli_help
     procedure, public             :: parse    => cli_parse_arguments
     procedure, public             :: get_iarg => cli_get_iarg
     procedure, public             :: get_farg => cli_get_farg
     procedure, public             :: get_rarg => cli_get_rarg
     procedure, public             :: get_sarg => cli_get_sarg
     procedure, public             :: get_iargf => cli_get_iargf 
     procedure, public             :: get_fargf => cli_get_fargf 
     procedure, public             :: get_rargf => cli_get_rargf 
     generic  , public             :: get      => get_iarg, get_farg, get_rarg, get_sarg
     procedure, public             :: list     => cli_list
     procedure, public             :: present  => cli_present_arg
  end type cli_type


public cli_type

contains

  subroutine cli_init(self, title, description)
    implicit none
    class(cli_type)           :: self
    character(*), optional        :: title, description

    if(present(title))       self%title       = title
    if(present(description)) self%description = description
    self%na = 0
    return
  end subroutine cli_init

  subroutine cli_help(self)
    implicit none
    class(cli_type) :: self

    integer :: i

    write(*,'(" ")')
    write(*,'(60("="))')
    write(*,'(" ")')
    write(*,'(" ",A)') self%title
    write(*,'(" ",A)') self%description
    write(*,'(" ")')
    write(*,'(" ")')

    write(*,'(" Usage:")')
    write(*,'(" ")')
    write(*,'(" $./",A)',advance='no')self%title
    do i=1, self%na
       write(*,'("   ",A)', advance='no')self%arg_list(i)%arg%switch
    end do
    write(*,'(" ")')
    write(*,'(" ")')
    write(*,'(" Where ")')
    write(*,'(" ")')
    do i=1, self%na
       write(*,'("   ",A)')self%arg_list(i)%arg%switch
       if(self%arg_list(i)%arg%description .ne. '') call print_description(&
            self%arg_list(i)%arg%description)
       select type (st=>self%arg_list(i)%arg)
       type is (iarg_type)
          write(*,'("      Type is Integer.")')
          write(*,'("      Default=",i4)')st%def
       type is (rarg_type)
          write(*,'("      Type is Real.")')
          write(*,'("      Default=",f7.4)')st%def
       type is (farg_type)
          write(*,'("      Type is Flag.")')
          write(*,'("      Default=.false.")')
       type is (sarg_type)
          write(*,'("      Type is String.")')
          write(*,'("      Default=",a)')st%def
       end select
          write(*,'(" ")')
    end do

    write(*,'("    -h,--help")')
    write(*,'("       Print this help message.")')
    write(*,'(" ")')
    write(*,'(60("="))')


    return
  end subroutine cli_help

  subroutine print_description(str)
    character(len=*) :: str
    integer :: i, i1, i2
    i1 = 1
    do
       i = index(str(i1:len(str)), '\n')
       if(i.eq.0) then
          i2 = len(str)
       else
          i2 = i1+i-2
       end if
       write(*,'(6x,a)')str(i1:i2)
       if(i2.eq.len(str)) exit
       i1 = i2+3
       cnt = cnt+1
    end do
    
    return
  end subroutine print_description
  
  subroutine cli_add_iarg(self, switch, description, def, req)
    implicit none
    class(cli_type), target :: self
    character(*)                :: switch
    character(*)                :: description
    integer                     :: def
    logical, optional           :: req

    integer                     :: id
    type(iarg_type)             :: iarg

    if (self%added(switch)) then
       write(*,'("Option ",a," was already added. Skipping")')switch
       return
    end if
       
    self%na = self%na+1
    id = self%na

    iarg%switch           = switch
    iarg%present          = .false.
    iarg%description      = description
    iarg%def              = def
    iarg%val              = def
    if(present(req)) then
       iarg%required = req
    else
       iarg%required = .false.
    end if
    self%arg_list(id)%arg = iarg
    
    return
  end subroutine cli_add_iarg

  subroutine cli_add_rarg(self, switch, description, def, req)
    implicit none
    class(cli_type), target :: self
    character(*)                :: switch
    character(*)                :: description
    real(real32)                :: def
    logical, optional           :: req

    integer                     :: id
    type(rarg_type)             :: rarg

    if (self%added(switch)) then
       write(*,'("Option ",a," was already added. Skipping")')switch
       return
    end if
       
    self%na = self%na+1
    id = self%na

    rarg%switch      = switch
    rarg%present     = .false.
    rarg%description = description
    rarg%def         = def
    rarg%val         = def
    if(present(req)) then
       rarg%required = req
    else
       rarg%required = .false.
    end if
    self%arg_list(id)%arg = rarg
    
    return
  end subroutine cli_add_rarg
  
  subroutine cli_add_farg(self, switch, description, def, req)
    implicit none
    class(cli_type), target :: self
    character(*)                :: switch
    character(*)                :: description
    logical                     :: def
    logical, optional           :: req

    integer                     :: id
    type(farg_type)             :: farg

    if (self%added(switch)) then
       write(*,'("Option ",a," was already added. Skipping")')switch
       return
    end if
       
    self%na = self%na+1
    id = self%na

    farg%switch           = switch
    farg%present          = .false.
    farg%description      = description
    farg%def              = .false.
    farg%val              = .false.
    if(present(req)) then
       farg%required = req
    else
       farg%required = .false.
    end if
    self%arg_list(id)%arg = farg
    
    return
  end subroutine cli_add_farg

  subroutine cli_add_sarg(self, switch, description, def, req)
    implicit none
    class(cli_type), target :: self
    character(*)                :: switch
    character(*)                :: description
    character(*)                :: def
    logical, optional           :: req

    integer                     :: id
    type(sarg_type)             :: sarg

    if (self%added(switch)) then
       write(*,'("Option ",a," was already added. Skipping")')switch
       return
    end if
       
    self%na = self%na+1
    id = self%na

    sarg%switch           = switch
    sarg%present          = .false.
    sarg%description      = description
    sarg%def              = def
    sarg%val              = def
    if(present(req)) then
       sarg%required = req
    else
       sarg%required = .false.
    end if
    self%arg_list(id)%arg = sarg
    
    return
  end subroutine cli_add_sarg

  function cli_present_arg(self, switch)
    implicit none

    logical             :: cli_present_arg
    class(cli_type) :: self
    character(*)        :: switch

    integer            :: i

    cli_present_arg = .false.
    
    do i=1, self%na
       if (self%arg_list(i)%arg%switch .eq. switch) then
          cli_present_arg = self%arg_list(i)%arg%present
          return
       end if
    end do

    return
  end function cli_present_arg

  function cli_added(self, switch)
    implicit none

    logical             :: cli_added
    class(cli_type) :: self
    character(*)        :: switch

    integer             :: i

    cli_added = .false.
    
    do i=1, self%na
       if (self%arg_list(i)%arg%switch .eq. switch) then
          cli_added = .true.
          return
       end if
    end do

    return
  end function cli_added

  subroutine cli_parse_arguments(self, info)

    implicit none
    class(cli_type)  :: self
    integer, optional    :: info

    integer              :: nargs, i, j, len
    character(LEN=80)    :: arg
    logical              :: notfound
    character(len=:), allocatable :: aed

    ! error management
    integer                       :: err
    character(len=*), parameter   :: name='cli_parse_arguments'
    
    err = 0
    
    nargs = command_argument_count()
    info = 0
    
    ! first, check if -h or --help is present
    do i=1, nargs
       call get_command_argument(i,value=arg)
       if((arg.eq.'-h').or.(arg.eq.'--help')) then
          call self%help()
          info = -1
          return
       end if
    end do

    i = 1
    args: do
       if(i.gt.nargs) exit
       call get_command_argument(i,value=arg, length=len)
       notfound = .true.
       do j=1, self%na
          if(arg(1:len).eq.self%arg_list(j)%arg%switch) then
             notfound = .false.
             if (self%arg_list(j)%arg%present) then
                write(*,'("Option ",a," has already been provided once.")')self%arg_list(j)%arg%switch
                write(*,'("Skipping further occurrences.")')
             else
                select type (st=>self%arg_list(j)%arg)
                type is (iarg_type)
                   if(i+1 .gt. nargs) then
                      call self%help()
                      return
                   end if
                   call get_command_argument(i+1,value=arg, length=len)
                   read(arg(1:len),*) st%val
                   st%present = .true.
                   i = i+2
                type is (rarg_type)
                   if(i+1 .gt. nargs) then
                      call self%help()
                      return
                   end if
                   call get_command_argument(i+1,value=arg, length=len)
                   read(arg(1:len),*) st%val
                   st%present = .true.
                   i = i+2
                type is (sarg_type)
                   if(i+1 .gt. nargs) then
                      call self%help()
                      return
                   end if
                   call get_command_argument(i+1,value=arg, length=len)
                   st%val     = arg(1:len)
                   st%present = .true.
                   i = i+2
                type is (farg_type)
                   st%present = .true.
                   st%val     = .true.
                   i = i+1
                end select
             end if

             cycle args
          end if
       end do
       if(notfound) then
          call self%help()
          return
       end if
    end do args

    do i=1, self%na
       if(self%arg_list(i)%arg%required .and. (.not.self%arg_list(i)%arg%present)) then
          call self%help()
          call self%help()
          aed = 'Command line argument '//self%arg_list(i)%arg%switch//' is required but was not provided.'
          return
       end if
    end do

10  continue
    if(present(info)) info = err
    
    return
  end subroutine cli_parse_arguments

  subroutine cli_list(self)
    implicit none
    class(cli_type) :: self
    integer             :: i

    write(*,'(" ")')
    write(*,'(" Resume of parameters values:")')
    do i=1, self%na
       select type (st=>self%arg_list(i)%arg)
       type is (iarg_type)
          write(*,'("   ",A," is ",i0)')st%switch, st%val
       type is (rarg_type)
          write(*,'("   ",A," is ",f0.3)')st%switch, st%val
       type is (farg_type)
          write(*,'("   ",A," is ",l)')st%switch, st%val
       end select
    end do
    write(*,'(" ")')

    return
  end subroutine cli_list


  subroutine cli_get_iarg(self, switch, ival)
    implicit none
    class(cli_type) :: self
    character(*)        :: switch
    integer             :: ival

    integer             :: i

    if(.not. self%added(switch)) then
       write(*,'("Unknown command line argument ",A)')switch
    end if
       
    do i=1, self%na
       if (self%arg_list(i)%arg%switch .eq. switch) then
          select type (st=>self%arg_list(i)%arg)
          type is (iarg_type)
             ival = st%val
          class default
             write(*,'("Type mismatch for argument ",A)') switch
          end select
       end if
    end do

    return

  end subroutine cli_get_iarg

  function cli_get_iargf(self, switch)
    class(cli_type) :: self
    character(*)        :: switch
    integer             :: cli_get_iargf

    call cli_get_iarg(self, switch, cli_get_iargf)
    return
  end function cli_get_iargf


  subroutine cli_get_rarg(self, switch, rval)
    implicit none
    class(cli_type) :: self
    character(*)        :: switch
    real(real32)        :: rval

    integer             :: i

    if(.not. self%added(switch)) then
       write(*,'("Unknown command line argument ",A)')switch
    end if
       
    do i=1, self%na
       if (self%arg_list(i)%arg%switch .eq. switch) then
          select type (st=>self%arg_list(i)%arg)
          type is (rarg_type)
             rval = st%val
          class default
             write(*,'("Type mismatch for argument ",A)') switch
          end select
       end if
    end do

    return

  end subroutine cli_get_rarg

  subroutine cli_get_sarg(self, switch, sval)
    implicit none
    class(cli_type)           :: self
    character(*)                  :: switch
    character(len=:), allocatable :: sval

    integer                       :: i

    if(.not. self%added(switch)) then
       write(*,'("Unknown command line argument ",A)')switch
    end if
       
    do i=1, self%na
       if (self%arg_list(i)%arg%switch .eq. switch) then
          select type (st=>self%arg_list(i)%arg)
          type is (sarg_type)
             sval = st%val
          class default
             write(*,'("Type mismatch for argument ",A)') switch
          end select
       end if
    end do

    return

  end subroutine cli_get_sarg

  function cli_get_rargf(self, switch)
    class(cli_type) :: self
    character(*)        :: switch
    real(real32)        :: cli_get_rargf

    call cli_get_rarg(self, switch, cli_get_rargf)
    return
  end function cli_get_rargf



  subroutine cli_get_farg(self, switch, fval)
    implicit none
    class(cli_type) :: self
    character(*)        :: switch
    logical             :: fval

    integer             :: i

    if(.not. self%added(switch)) then
       write(*,'("Unknown command line argument ",A)')switch
    end if
       
    do i=1, self%na
       if (self%arg_list(i)%arg%switch .eq. switch) then
          select type (st=>self%arg_list(i)%arg)
          type is (farg_type)
             fval = st%val
          class default
             write(*,'("Type mismatch for argument ",A)') switch
          end select
       end if
    end do

    return

  end subroutine cli_get_farg

  function cli_get_fargf(self, switch)
    class(cli_type) :: self
    character(*)        :: switch
    logical             :: cli_get_fargf

    call cli_get_farg(self, switch, cli_get_fargf)
    return
  end function cli_get_fargf

  

end module cli_mod

