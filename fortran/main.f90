program main

  use iso_fortran_env
  use utils_mod
  use laqps_rnd_mod
  
  implicit none

  real(real64), allocatable :: a(:,:), epss(:), work(:), u(:,:), vt(:,:), s(:)
  integer, allocatable      :: seed(:)
  integer                   :: m, n, b, p, lw, info, rk, len, rkd, rks, tl, i, lwork
  real(real64)              :: nrma, eps, kappa, aa, bb, dd, err, time, dlamch
  character(LEN=20)         :: str
  character(LEN=12)         :: typ
  character(len=:), allocatable :: mat
  integer                   :: verb=2
  
  call get_cla(m, n, epss, mat, kappa, aa, bb, dd, b, p, verb)

  tl = len(mat)
  
  allocate(a(m,n))

  select case (mat(1:tl))
  case ('randsvd')
     call randsvd(a, kappa)
  case ('foxgood')
     call foxgood(a)
  case ('hilbert')
     call hilbert(a)
  case ('baart')
     call baart(a)
  case ('kahan')
     call kahan(a)
  case ('toeplitz')
     call toeplitz(a)
  case ('heat')
     call heat(a)
  case ('phillips')
     call phillips(a)
  case ('gravity')
     call gravity(a, aa, bb, dd)
     write(str,'("gravity_",f4.2)')dd
     mat=trim(str)
  case default
  end select

  ! call print_mat(a(1:5,1:5),'es10.4',unit=6)

  if(verb.eq.2) call print_sep
  if(verb.eq.2) call print_header()
  if(verb.ge.2) call print_sep

  eps = -10.d0
  
  ! call test_double_full(a, eps, time, err, rkd, rks)
  ! call write_result(mat, m, n, eps, 'double_full', rkd, rks, time, err)

  ! call test_single_full(a, eps, time, err, rkd, rks)
  ! call write_result(mat, m, n, eps, 'single_full', rkd, rks, time, err)

  call test_double_piv(a, eps, time, err, rkd, rks)
  call write_result(mat, m, n, eps, 'double_full', rkd, rks, time, err)

  call test_single_piv(a, eps, time, err, rkd, rks)
  call write_result(mat, m, n, eps, 'single_full', rkd, rks, time, err)

  if(verb.ge.1) call print_sep
  do i=1,size(epss)
     eps = epss(i)

     call test_double_piv(a, eps, time, err, rkd, rks)
     call write_result(mat, m, n, eps, ' double_piv ', rkd, rks, time, err)
     ! write(20,*)' '
     call test_mixed_piv(a, eps, time, err, rkd, rks)
     call write_result(mat, m, n, eps, '  mixed_piv', rkd, rks, time, err)
     
     ! call test_single_piv(a, eps, time, err, rkd, rks)
     ! call write_result(mat, m, n, eps, 'single_piv', rkd, rks, time, err)
     
     call test_double_rnd(a, eps, time, err, rkd, rks)
     call write_result(mat, m, n, eps, ' double_rnd', rkd, rks, time, err)
     ! write(20,*)' '
     
     call test_mixed_rnd(a, eps, time, err, rkd, rks)
     call write_result(mat, m, n, eps, '  mixed_rnd', rkd, rks, time, err)
     
     ! call test_single_rnd(a, eps, time, err, rkd, rks)
     ! call write_result(mat, m, n, eps, ' single_rnd', rkd, rks, time, err)
     if(verb.ge.1) call print_sep
  end do

  ! lwork = 32*max(m,n)
  ! allocate(work(lwork), s(min(m,n)), u(1,1), vt(1,1))
  
  ! call dgesvd( 'n', 'n', m, n, a, m, s, u, m, vt, m, work, lwork, info )
  ! do i=1,min(m,n)
     ! write(20,*)s(i)
  ! end do
  
  stop


contains

  subroutine test_double_full(a, eps, time, err, rkd, rks)
    implicit none

    real(real64) :: a(:,:)
    real(real64) :: eps, time, err
    integer      :: rkd, rks

    real(real64), allocatable :: qrd(:,:), taud(:), wd(:)
    integer, allocatable      :: jpvt(:)
    integer                   :: m, n, lw
    real(real64)              :: epstr, epssw, us, ud, nrma
    logical                   :: upd
    real(real64)              :: dnrm2, dlamch
    real(real32)              :: slamch
    integer(8)                :: ts, te, tr
    
    m = size(a,1)
    n = size(a,2)

    lw =  2*n+( n+1 )*128
    
    allocate(qrd(m,n), taud(n), wd(lw), jpvt(n))

    call reset_stats()
    
    ud    = dlamch('Epsilon')*2.d0
    us    = real(slamch('Epsilon'),real64)*2.d0
    nrma  = dnrm2(m*n, a(1,1), 1)
    epstr = eps*nrma
    epssw = epstr/us

    jpvt = 0
    qrd  = a
    rk   = n
    taud = 0.d0
    
    call system_clock(ts)
    call dgeqp3(m, n,             & ! m, n
                qrd(1,1), m,      & ! a, lda
                jpvt(1),          & ! jpvt
                taud(1),          & ! tau
                wd(1), lw,        & ! work, lwork
                info)               ! info
    call system_clock(te, tr)
    time = real(te-ts)/real(tr)
    rkd = min(m,n)
    rks = 0

    err = check_err(a, qrd, taud, jpvt, rkd)
    rks = 0
    
    return
  end subroutine test_double_full

  subroutine test_single_full(a, eps, time, err, rkd, rks)
    implicit none

    real(real64) :: a(:,:)
    real(real64) :: eps, time, err
    integer      :: rkd, rks

    real(real32), allocatable :: qrs(:,:), taus(:), ws(:)
    real(real64), allocatable :: qrd(:,:), taud(:)
    integer, allocatable      :: jpvt(:)
    integer                   :: m, n, lw
    real(real32)              :: epstr, epssw
    real(real64)              :: us, ud, nrma
    logical                   :: upd
    real(real64)              :: dnrm2, dlamch
    real(real32)              :: snrm2, slamch
    integer(8)                :: ts, te, tr
    
    m = size(a,1)
    n = size(a,2)

    lw =  2*n+( n+1 )*128
    
    allocate(qrs(m,n), taus(n), ws(lw), jpvt(n))
    allocate(qrd(m,n), taud(n))
    
    ud    = dlamch('Epsilon')*2.d0
    us    = real(slamch('Epsilon'),real64)*2.d0
    nrma  = dnrm2(m*n, a(1,1), 1)
    epstr = real(eps*nrma,real32)
    epssw = epstr/us

    jpvt = 0
    qrs  = real(a,real32)
    rk   = n
    taus = 0.d0
    
    call system_clock(ts)
    call sgeqp3(m, n,             & ! m, n
                qrs(1,1), m,      & ! a, lda
                jpvt(1),          & ! jpvt
                taus(1),          & ! tau
                ws(1), lw,        & ! work, lwork
                info)               ! info
    call system_clock(te, tr)
    time = real(te-ts)/real(tr)
    rkd = min(m,n)
    rks = 0
    taud = real(taus,real64)
    qrd  = real(qrs,real64)
    
    err = check_err(a, qrd, taud, jpvt, rkd)
    rks = 0
    
    return
  end subroutine test_single_full

  subroutine test_double_piv(a, eps, time, err, rkd, rks)
    implicit none

    real(real64) :: a(:,:)
    real(real64) :: eps, time, err
    integer      :: rkd, rks

    real(real64), allocatable :: qrd(:,:), taud(:), wd(:)
    integer, allocatable      :: jpvt(:)
    integer                   :: m, n, lw
    real(real64)              :: epstr, epssw, us, ud, nrma
    logical                   :: upd
    real(real64)              :: dnrm2, dlamch
    real(real32)              :: slamch
    integer(8)                :: ts, te, tr
    
    m = size(a,1)
    n = size(a,2)

    lw =  2*n+( n+1 )*128

    call reset_stats()
    
    allocate(qrd(m,n), taud(n), wd(lw), jpvt(n))
    
    ud    = dlamch('Epsilon')*2.d0
    us    = real(slamch('Epsilon'),real64)*2.d0
    nrma  = dnrm2(m*n, a(1,1), 1)
    epstr = eps*nrma
    epssw = epstr/us

    jpvt = 0
    qrd  = a
    rk   = n
    taud = 0.d0

    call system_clock(ts)
    call dgeqp3_truncated(m, n, b,            & ! m, n, b
                          qrd(1,1), m,        & ! a, lda
                          jpvt(1),            & ! jpvt
                          taud(1),            & ! tau
                          wd(1), lw,          & ! work, lwork
                          rkd, epstr, 0.d0,   &
                          min(m,n)        ,   & ! maxrank
                          upd, .false., info)   ! upd, reln, info
    call system_clock(te, tr)
    time = real(te-ts)/real(tr)
    err = check_err(a, qrd, taud, jpvt, rkd)
    ! write(*,*)'AI: ',(dai+sai)/(dflops+sflops),dai/dflops,sai/sflops
    rks = 0

    return
  end subroutine test_double_piv

  subroutine test_single_piv(a, eps, time, err, rkd, rks)
    implicit none

    real(real64) :: a(:,:)
    real(real64) :: eps, time, err
    integer      :: rkd, rks

    real(real64), allocatable :: qrd(:,:), taud(:)
    real(real32), allocatable :: qrs(:,:), taus(:), ws(:)
    integer, allocatable      :: jpvt(:)
    integer                   :: m, n, lw
    real(real64)              :: us, ud, nrma
    real(real32)              :: epstr, epssw
    logical                   :: upd
    real(real64)              :: dnrm2, dlamch
    real(real32)              :: slamch
    integer(8)                :: ts, te, tr
    
    m = size(a,1)
    n = size(a,2)

    lw =  2*n+( n+1 )*128
    
    call reset_stats()
    
    allocate(qrs(m,n), taus(n), ws(lw), jpvt(n))
    allocate(qrd(m,n), taud(n))
    
    ud    = dlamch('Epsilon')*2.d0
    us    = real(slamch('Epsilon'),real64)*2.d0
    nrma  = dnrm2(m*n, a(1,1), 1)
    epstr = real(eps*nrma,real32)
    epssw = real(epstr/us,real32)

    jpvt = 0
    qrs  = a
    rk   = n
    taus = 0.d0
    
    call system_clock(ts)
    call sgeqp3_truncated(m, n, b,            & ! m, n, b
                          qrs(1,1), m,        & ! a, lda
                          jpvt(1),            & ! jpvt
                          taus(1),            & ! tau
                          ws(1), lw,          & ! work, lwork
                          rkd, epstr, 0.e0,   &
                          min(m,n)        ,   & ! maxrank
                          upd, .false., info)   ! info
    call system_clock(te, tr)
    time = real(te-ts)/real(tr)
    qrd  = real(qrs,real64)
    taud = real(taus,real64)
    err = check_err(a, qrd, taud, jpvt, rkd)
    ! write(*,*)'AI: ',(dai*dflops+sai*sflops)/(dflops+sflops)
    rks = 0
    
    return
  end subroutine test_single_piv

  subroutine test_single_rnd(a, eps, time, err, rkd, rks)
    implicit none

    real(real64) :: a(:,:)
    real(real64) :: eps, time, err
    integer      :: rkd, rks

    real(real64), allocatable :: qrd(:,:), taud(:)
    real(real32), allocatable :: qrs(:,:), taus(:), ws(:), so(:,:), ss(:,:)
    integer, allocatable      :: jpvt(:)
    integer                   :: m, n, lw
    real(real64)              :: us, ud, nrma
    real(real32)              :: epstr, epssw
    logical                   :: upd
    real(real64)              :: dnrm2, dlamch
    real(real32)              :: slamch
    integer(8)                :: ts, te, tr
    
    m = size(a,1)
    n = size(a,2)

    lw =  2*n+( n+1 )*128
    
    call reset_stats()
    
    allocate(qrs(m,n), taus(n), ws(lw), jpvt(n))
    allocate(qrd(m,n), taud(n))
    
    ud    = dlamch('Epsilon')*2.d0
    us    = real(slamch('Epsilon'),real64)*2.d0
    nrma  = dnrm2(m*n, a(1,1), 1)
    epstr = real(eps*nrma,real32)
    epssw = real(epstr/us,real32)

    jpvt = 0
    qrs  = a
    rk   = n
    taus = 0.d0
    
    call system_clock(ts)
    call slaqps_rnd_truncated(qrs, m, b, p, taus, jpvt, &
         rkd, epstr, 0.e0, upd, so, ss)
    call system_clock(te, tr)
    time = real(te-ts)/real(tr)
    qrd  = real(qrs,real64)
    taud = real(taus,real64)
    err = check_err(a, qrd, taud, jpvt, rkd)
    ! write(*,*)'AI: ',(dai*dflops+sai*sflops)/(dflops+sflops)
    rks = 0
    
    return
  end subroutine test_single_rnd

  subroutine test_double_rnd(a, eps, time, err, rkd, rks)
    implicit none

    real(real64) :: a(:,:)
    real(real64) :: eps, time, err
    integer      :: rkd, rks

    real(real64), allocatable :: qrd(:,:), taud(:), wd(:)
    integer, allocatable      :: jpvt(:)
    integer                   :: m, n, lw
    real(real64)              :: epstr, epssw, us, ud, nrma
    logical                   :: upd
    real(real64)              :: dnrm2, dlamch
    real(real32)              :: slamch
    integer(8)                :: ts, te, tr
    real(real64), allocatable :: do(:,:), ds(:,:)
    
    m = size(a,1)
    n = size(a,2)

    lw =  2*n+( n+1 )*128
    
    call reset_stats()
    
    allocate(qrd(m,n), taud(n), wd(lw), jpvt(n))
    
    ud    = dlamch('Epsilon')*2.d0
    us    = real(slamch('Epsilon'),real64)*2.d0
    nrma  = dnrm2(m*n, a(1,1), 1)
    epstr = eps*nrma
    ! epssw = epstr/sqrt(real(n,real64))/us
    epssw = epstr/us

    jpvt = 0
    qrd  = a
    rk   = n
    taud = 0.d0
    
    call system_clock(ts)
    call dlaqps_rnd_truncated(qrd, m, b, p, taud, jpvt, &
         rkd, epstr, 0.d0, upd, do, ds)
    call system_clock(te, tr)
    time = real(te-ts)/real(tr)
    err = check_err(a, qrd, taud, jpvt, rkd)
    ! write(*,*)'AI: ',(dai+sai)/(dflops+sflops),dai/dflops,sai/sflops
    rks = 0
    
    return
  end subroutine test_double_rnd



  subroutine test_mixed_piv(a, eps, time, err, rkd, rks)
    implicit none

    real(real64) :: a(:,:)
    real(real64) :: eps, time, err
    integer      :: rkd, rks

    real(real64), allocatable :: qrd(:,:), taud(:), wd(:)
    real(real32), allocatable :: qrs(:,:), taus(:), ws(:)
    integer, allocatable      :: jpvt(:)
    integer                   :: m, n, lw
    real(real64)              :: epstr, epssw, us, ud, nrma
    logical                   :: upd
    real(real64)              :: dnrm2, dlamch
    real(real32)              :: slamch
    integer(8)                :: ts, te, tr
    
    m = size(a,1)
    n = size(a,2)

    lw =  2*n+( n+1 )*128
    
    call reset_stats()
    
    allocate(qrd(m,n), taud(n), wd(lw), jpvt(n))
    allocate(qrs(m,n), taus(n))
    
    ud    = dlamch('Epsilon')*2.d0
    us    = real(slamch('Epsilon'),real64)*2.d0
    nrma  = dnrm2(m*n, a(1,1), 1)
    epstr = eps*nrma
    epssw = epstr/us

    jpvt = 0
    qrd  = a
    rk   = n
    taud = 0.d0
    
    call system_clock(ts)
    call dsgeqp3_truncated(m, n, b,          &
                           qrd(1,1), m,      &
                           qrs(1,1), m,      &
                           jpvt(1),          &
                           taud(1), taus(1), &
                           wd(1), lw,        &
                           rkd, rks, eps,    &
                           info)
    call system_clock(te, tr)
    time = real(te-ts)/real(tr)
    err = check_err_ds(a, qrd, qrs, taud, taus, jpvt, rkd, rks)  
    ! write(*,*)'AI: ',(dai+sai)/(dflops+sflops),dai/dflops,sai/sflops
    
    return
  end subroutine test_mixed_piv


  subroutine test_mixed_rnd(a, eps, time, err, rkd, rks)
    implicit none

    real(real64) :: a(:,:)
    real(real64) :: eps, time, err
    integer      :: rkd, rks

    real(real64), allocatable :: qrd(:,:), taud(:), wd(:)
    real(real32), allocatable :: qrs(:,:), taus(:), ws(:)
    integer, allocatable      :: jpvt(:)
    integer                   :: m, n, lw
    real(real64)              :: epstr, epssw, us, ud, nrma
    logical                   :: upd
    real(real64)              :: dnrm2, dlamch
    real(real32)              :: slamch
    integer(8)                :: ts, te, tr
    
    m = size(a,1)
    n = size(a,2)

    lw =  2*n+( n+1 )*128
    
    call reset_stats()
    
    allocate(qrd(m,n), taud(n), wd(lw), jpvt(n))
    allocate(qrs(m,n), taus(n))
    
    ud    = dlamch('Epsilon')*2.d0
    us    = real(slamch('Epsilon'),real64)*2.d0
    nrma  = dnrm2(m*n, a(1,1), 1)
    epstr = eps*nrma
    epssw = epstr/us

    jpvt = 0
    qrd  = a
    rk   = n
    taud = 0.d0
    
    call system_clock(ts)
    call dslaqps_rnd_truncated(qrd, qrs, &
                               b, p, &
                               taud, taus, &
                               jpvt, &
                               rkd, rks, eps)
    call system_clock(te, tr)
    time = real(te-ts)/real(tr)
    err = check_err_ds(a, qrd, qrs, taud, taus, jpvt, rkd, rks)  
    ! write(*,*)'AI: ',(dai+sai)/(dflops+sflops),dai/dflops,sai/sflops
    
    return
  end subroutine test_mixed_rnd




   subroutine get_cla(m, n, epss, mat, kappa, aa, bb, dd, b, p, verb)
     use cli_mod
     implicit none
     
     integer            :: m, n, b, p, verb
     real(real64), allocatable :: epss(:)
     real(real64)       :: aa, bb, dd, kappa 
     real(real32)       :: saa, sbb, sdd, skappa
     character(len=:), allocatable :: mat, seps
     type(cli_type) :: cli
     integer            :: l, idx, cnt, i
     
     call cli%init(title='main', description='A test program for the mixed precision RRQR factorization')
     call cli%add('-m'    , 'Number of rows in the matrix.'                              , def=1024            )
     call cli%add('-n'    , 'Number of cols in the matrix.'                              , def=1024            )
     call cli%add('-eps'  , 'Truncation threshold.'                                      , def='0.d0'          )
     call cli%add('-mat'  , 'Matrix type.'                                               , def='randsvd'       )
     call cli%add('-k'    , 'kappa parameter for randsvd matrix.'                        , def=1e15            )
     call cli%add('-aa'   , 'aa parameter for gravity matrix.'                           , def=0.e0            )
     call cli%add('-bb'   , 'bb parameter for gravity matrix.'                           , def=1.e0            )
     call cli%add('-dd'   , 'dd parameter for gravity matrix.'                           , def=0.15e0          )
     call cli%add('-b'    , 'Panel rowsize.'                                             , def=64              )
     call cli%add('-p'    , 'Oversampling.'                                              , def=4               )
     call cli%add('-v'  , 'Verbosity: 2=batch w/ header, 1=batch w/o header.'            , def=2               )
     
     call cli%parse(info)
     
     call cli%get('-m'    , m)
     call cli%get('-n'    , n)
     call cli%get('-eps'  , seps)
     call cli%get('-mat'  , mat)
     call cli%get('-k'    , skappa); kappa=real(skappa,real64)
     call cli%get('-aa'   , saa); aa=real(saa,real64)
     call cli%get('-bb'   , sbb); bb=real(sbb,real64)
     call cli%get('-dd'   , sdd); dd=real(sdd,real64)
     call cli%get('-b'    , b)
     call cli%get('-p'    , p)
     call cli%get('-v'    , verb)

     ! process seps (comma-separated list of tols)
     idx = 1
     l   = len(seps)
     cnt = 1
     do
        i = index(seps(idx:l), ',')
        if(i.eq.0) exit
        idx = idx+i
        cnt = cnt+1
     end do

     allocate(epss(cnt))
     idx = 1
     l   = len(seps)
     cnt = 1
     do
        i = index(seps(idx:l), ',')
        if(i.eq.0) then
           read(seps(idx:l),*)epss(cnt)
           exit
        else
           read(seps(idx:idx+i-1),*)epss(cnt)
        end if
        idx = idx+i
        cnt = cnt+1
     end do

     return
     
   end subroutine get_cla
   
  

  subroutine write_result(mat, m, n, eps, test, rkd, rks, time, err)

    implicit none

    character*(*) :: mat, test
    integer :: m, n, rkd, rks, ierr
    real(real64) :: eps, time, err
    character(LEN=10) :: str

    write(*,'("| ",a12,x)', advance='no')mat
    write(*,'("| ",i4,x)', advance='no')m
    write(*,'("| ",i4,x)', advance='no')n
    write(*,'("| ",es8.1,x)', advance='no')eps
    write(*,'("| ",a11,x)', advance='no')test
    write(*,'("| ",i4,x)', advance='no')rkd
    write(*,'("| ",i4,x)', advance='no')rks
    write(*,'("| ",f7.3,x)', advance='no')time
    write(*,'("| ",es10.3,x)', advance='no')err
    write(*,'("| ",f8.1,x)', advance='no')real(dflops)/1e6 
    write(*,'("| ",f8.1,x)', advance='no')real(sflops)/1e6 
    write(*,'("|")')
    
    return
  end subroutine write_result

   subroutine print_header()
     write(*,'("| Matrix       |    M |    N |      eps ")', advance='no')
     write(*,'("|       test  | rkd  | rks  | time    ")', advance='no')
     write(*,'("|      err   ")', advance='no')
     write(*,'("|   dflops ")', advance='no')
     write(*,'("|   sflops |")')
   end subroutine print_header

   subroutine print_sep
     write(*,'("|--------------|------|------|----------")', advance='no')
     write(*,'("|-------------|------|------|---------")', advance='no')
     write(*,'("|------------")', advance='no')
     write(*,'("|----------")', advance='no')
     write(*,'("|----------|")')
   end subroutine print_sep

   
end program main
