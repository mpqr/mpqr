module utils_mod

  use iso_fortran_env

  integer(8)   :: dflops, sflops
  real(real64) :: dai, sai

contains

  subroutine reset_stats()
    implicit none
    dflops = 0
    sflops = 0
    dai    = 0.d0
    sai    = 0.d0
    return
  end subroutine reset_stats

  subroutine randsvd(a, k)
    implicit none
    real(real64) :: a(:,:)
    real(real64) :: k

    integer      :: m, n, s, i, info, lw
    real(real64) :: p, f, u, v
    real(real64), allocatable :: d(:), w(:)
    integer      :: iseed(4)=(/1, 1, 1, 1/)

    m = size(a,1)
    n = size(a,2)
    p = real(min(m,n),real64)

    a = 0.d0

    f = k**(-1/(p-1))

    lw = max(m+n, 32*max(m,n))

    allocate(d(min(m,n)), w(lw))

    do i=1, min(m,n)
       d(i) = f**(real(i,real64)-1)
    end do

    call dlagge( m, n, m-1, n-1, d, a, m, iseed, w, info )


    ! call print_mat(a, 'f20.15', unit=20)

    ! d = 0.d0
    ! call dgesvd( 'n', 'n', m, n, a, m, d, u, 1, v, 1, w, lw, info )
    ! do i=1, min(m,n)
    ! write(*,'(f20.15)')d(i)
    ! end do


    return

  end subroutine randsvd



  subroutine print_mat(a, fmt, triu, unit)

    implicit none
    real(real64)      :: a(:,:)
    character(len=*)  :: fmt
    logical, optional :: triu
    integer, optional :: unit

    logical           :: itriu
    integer           :: i, j, n, m, iu, ln, h, k
    character(len=:), allocatable :: zfmt, dfmt


    n = size(a,2)
    m = size(a,1)

    if(present(triu)) then
       itriu = triu
    else
       itriu = .false.
    end if

    if(present(unit)) then
       iu = unit
    else
       iu = 6
    end if

    ln = len(fmt)

    if(fmt(1:1).eq.'f') then
       k = 2
    else if(fmt(1:1).eq.'e') then
       if(fmt(2:2).eq.'s') then
          k = 3
       else
          k = 2
       end if
    else
       write(*,'("Invalid format. Returning.")')
       return
    end if

    do h=k, ln
       if(fmt(h:h).eq.'.') exit
    end do

    zfmt = "("//fmt(k:h-1)//"x,x"//")"
    dfmt = "("//fmt//",x)"


    do i=1, m
       do j=1, n
          if(j.lt.i) then
             if(itriu) then
                write(iu,zfmt, advance='no')
             else
                write(iu,dfmt, advance='no')a(i,j)
             end if
          else
             write(iu,dfmt, advance='no')a(i,j)
          end if
       end do
       write(iu,'("")')
    end do

  end subroutine print_mat


  function check_err(a, qr, tau, jpvt, rk)

    implicit none 

    real(real64) :: a(:,:), qr(:,:), tau(:)
    integer      :: jpvt(:)
    integer      :: rk
    real(real64) :: check_err

    integer      :: m, n, i, j, info, lw, u=30
    real(real64), allocatable :: r(:,:), w(:), qxr(:,:)
    real(real64) :: dnrm2, nrma, nrmdiff


    m = size(a,1)
    n = size(a,2)

    r = qr(1:rk,1:n)

    ! zero out subdiag in r
    do i=1,rk
       do j=1, i-1
          r(i,j) = 0.d0
       end do
    end do

    ! permute cols of r
    call dlapmt(.false.,      & ! Forward
         rk, n,              & ! m, n
         r(1,1), rk,         & ! x, ldx
         jpvt(1))              ! k
    ! r(:,jpvt(1:n)) = r(:,1:n)

    ! build the economy-size q matrix
    lw = 64*n
    allocate(w(lw))
    call dorgqr(m, rk, rk, qr, m, tau, w, lw, info )

    ! call print_mat(a , 'f20.15',unit=20)
    ! call print_mat(r , 'f20.15',unit=u)
    ! write(*,'("-------------------------------------------------------- ")')
    ! call print_mat(qr(:,1:rk), 'f20.15',unit=u+1)
    ! u=u+10

    allocate(qxr(m,n))
    qxr = a
    ! compute QxR-A
    call dgemm('n', 'n', m, n, rk, &
         1.d0,               &
         qr(1,1), m,         &
         r(1,1), rk,         &
         -1.d0,               &
         qxr(1,1), m         )


    ! call print_mat(a, 'f8.5')
    ! write(*,'(" ")')
    ! call print_mat(qxr, 'f8.5')

    nrma    =  dnrm2(m*n,a,1)
    nrmdiff =  dnrm2(m*n,qxr,1)
    check_err = nrmdiff/nrma
    return
  end function check_err



  function check_err_ds(a, qrd, qrs, taud, taus, jpvt, rkd, rks)

    implicit none 

    real(real64) :: a(:,:), qrd(:,:), taud(:)
    real(real32) :: qrs(:,:), taus(:)
    integer      :: jpvt(:)
    integer      :: rkd, rks
    real(real64) :: check_err_ds

    integer      :: m, n, i, j, info, lw, rk
    real(real64), allocatable :: r(:,:), w(:), qxr(:,:)
    real(real64) :: dnrm2, nrma, nrmdiff

    m = size(a,1)
    n = size(a,2)
    rk = rkd+rks

    allocate(r(rkd+rks,n))
    r(1:rkd,1:n) = qrd(1:rkd,1:n)
    r(rkd+1:rk,rkd+1:n) = real(qrs(rkd+1:rk,rkd+1:n),real32)

    ! zero out subdiag in r
    do i=1,rk
       do j=1, i-1
          r(i,j) = 0.d0
       end do
    end do
    ! write(*,'(" ")')
    ! call print_mat(r, 'f9.6',triu=.true.)

    ! permute cols of r
    call dlapmt(.false.,      & ! Forward
         rk, n,              & ! m, n
         r(1,1), rk,         & ! x, ldx
         jpvt(1))              ! k
    ! r(:,jpvt(1:n)) = r(:,1:n)

    qrd(rkd+1:m,rkd+1:rk) = real(qrs(rkd+1:m,rkd+1:rk),real32)
    taud(rkd+1:rk) = real(taus(rkd+1:rk),real32)

    ! build the economy-size q matrix
    lw = 64*n
    allocate(w(lw))
    call dorgqr(m, rk, rk, qrd, m, taud, w, lw, info )

    allocate(qxr(m,n))
    qxr = a
    ! compute QxR-A
    call dgemm('n', 'n', m, n, rk, &
         1.d0,               &
         qrd(1,1), m,        &
         r(1,1), rk,         &
         -1.d0,              &
         qxr(1,1), m         )


    ! call print_mat(a, 'f8.5')
    ! write(*,'(" ")')
    ! call print_mat(qxr, 'f8.5')

    nrma    =  dnrm2(m*n,a,1)
    nrmdiff =  dnrm2(m*n,qxr,1)
    check_err_ds = nrmdiff/nrma
    ! write(*,'("||A-QR||/||A|| = ",es8.1)')nrmdiff/nrma
    return
  end function check_err_ds



  subroutine foxgood(a)
    implicit none
    real(real64) :: a(:,:)

    real(real64) :: h
    real(real64), allocatable :: t(:)

    integer :: m, n, i, j

    m = size(a,1)
    n = size(a,2)

    if(m.ne.n) then
       write(*,'("Foxgood must be a square matrix.")')
       return
    end if

    h = 1.d0/real(n,real64)

    allocate(t(n))

    do i=1, n
       t(i) = h*(real(i,real64)-0.5)
    end do

    do i=1, n
       do j=1, n
          a(i,j) = h*sqrt(t(i)**2 + t(j)**2)
       end do
    end do

    ! call print_mat(a, 'f8.6')

    return
  end subroutine foxgood

  subroutine baart(a)
    implicit none
    real(real64) :: a(:,:)

    integer :: m, n, i, j, n1, nh

    real(real64) :: pi, hs, ht, rn, co2, co3, c
    real(real64), allocatable :: ihs(:), f3(:), f1(:), f2(:)

    m = size(a,1)
    n = size(a,2)

    if(m.ne.n) then
       write(*,'("Baart must be a square matrix.")')
       return
    end if

    if(mod(m,2).ne.0) then
       write(*,'("Baart size must be even.")')
       return
    end if

    pi = 4.d0*atan(1.d0)
    rn = real(n,real64)

    hs  = pi/(2.d0*rn)
    ht  = pi/rn

    c   = 1.d0/(3.d0*sqrt(2.d0))
    a   = 0.d0

    allocate(ihs(n+1), f1(n), f2(n), f3(n))

    do i=1, n+1
       ihs(i) = real(i-1,real64)*hs
    end do

    n1  = n + 1
    nh  = n/2

    do i=1, n
       f3(i) = exp(ihs(i+1)) - exp(ihs(i))
    end do

    ! compute a
    do j=1, n
       f1 = f3
       co2 = cos((real(j,real64) - 0.5d0)*ht)
       co3 = cos(real(j,real64)*ht)
       do i=1, n
          f2(i) = (exp(ihs(i+1)*co2) - exp(ihs(i)*co2))/co2
       end do
       if(j==nh) then
          f3 = hs
       else
          do i=1, n
             f3(i) = (exp(ihs(i+1)*co3) - exp(ihs(i)*co3))/co3
          end do
       end if

       do i=1, n
          a(i,j) = c*(f1(i)+4.d0*f2(i) + f3(i))
       end do

    end do

    ! call print_mat(a, 'f9.5')
    return
  end subroutine baart

  subroutine hilbert(a)
    implicit none
    real(real64) :: a(:,:)

    integer :: m, n, i, j, n1, nh

    m = size(a,1)
    n = size(a,2)

    do i=1, m
       do j=1,n
          a(i,j) = 1.d0/(real(i+j,real64)-1.d0)
       end do
    end do

    ! call print_mat(a, 'f9.5')
    return
  end subroutine hilbert


  subroutine gravity(g, a, b, d)
    implicit none
    real(real64) :: g(:,:)
    real(real64) :: a, b, d

    integer :: m, n, i, j, n1, nh
    real(real64) :: dt, ds
    real(real64), allocatable :: tv(:), sv(:)

    m = size(g,1)
    n = size(g,2)

    if(m.ne.n) then
       write(*,'("Gravity must be a square matrix.")')
       return
    end if

    allocate(tv(n), sv(n))

    dt = 1.d0/real(n,real64)
    ds = (b - a)/real(n,real64)

    do i=1, n
       ! tv = dt .* (T[1:n;] .- one(T) ./ 2)
       tv(i) =     dt * (real(i,real64) - 0.5d0)
       ! sv = a .+ ds .* (T[1:n;] .- one(T) ./ 2)
       sv(i) = a + ds * (real(i,real64) - 0.5d0)
    end do

    do i=1,n
       do j=1,n
          ! A = dt .* d .* ones(T, n, n) ./ (d^2 .+ (Sm .- Tm).^2).^T(3/2)
          g(i,j) = dt*d*1.d0/((d*d + (sv(i)-tv(j))**2) ** (3.d0/2.d0))
       end do
    end do

    ! call print_mat(g, 'f9.5')

    ! stop
    return
  end subroutine gravity



  subroutine kahan(a, theta, pert)
    implicit none

    real(real64) :: a(:,:)
    real(real64), optional :: theta, pert

    integer      :: m, n, dim, i, j
    real(real64) :: s, c, dlamch
    real(real64) :: itheta, ipert, tau

    m = size(a,1)
    n = size(a,2)
    if(m.ne.n) then
       write(*,'("Kahan must be a square matrix.")')
       return
    end if

    itheta = 1.2d0
    if(present(theta)) itheta = theta

    ipert = 25.d0
    ! ipert = 1.d0
    if(present(pert)) ipert = pert

    s = sin(itheta)
    c = cos(itheta)

    a = 0.d0
    do i = 1,m
       do j = 1,n
          if(i.eq.j) then
             A(i,j) = s**(i-1)+ipert*2.d0*dlamch('Epsilon')*(m-i+1)
          else if (i.lt.j) then
             A(i,j) = -c*s**(i-1)
          end if
       end do
    end do
    tau=1d-4
    do j = 1,n
       a(:,j)=a(:,j)*((1-tau)**(n-j))
    end do
    ! tau=1d-6
    ! do j = 1,n
       ! a(:,j)=a(:,j)*((1-tau)**(j-1))
    ! end do

  end subroutine kahan

  subroutine toeplitz(a, vc, vr)
    implicit none

    real(real64) :: a(:,:)
    real(real64), optional, target :: vc(:), vr(:)

    real(real64), pointer :: ivc(:), ivr(:)
    integer :: m, n, i, j

    m = size(a,1)
    n = size(a,2)
    if(m.ne.n) then
       write(*,'("Toeplitz must be a square matrix.")')
       return
    end if

    if(present(vr)) then
       ivr => vr
    else
       allocate(ivr(m))
       ivr = (/(i, i=1,m,1)/)
    end if

    if(present(vc)) then
       ivc => vc
    else
       allocate(ivc(n))
       ivc = (/(i, i=1,n,1)/)
    end if

    if(m.ne.size(ivr) .or. n.ne.size(ivc)) then
       write(*,'("Toeplitz: incorrect size for vc/vr.")')
       return
    end if

    if(ivr(1) .ne. ivc(1)) then
       write(*,'("Toeplitz:  vc(1) must be == vr(1).")')
       return
    end if

    do i=1, m
       do j=1, n
          if(i.ge.j) then
             A(i,j) = ivc(i-j+1)
          else
             A(i,j) = ivr(j-i+1)
          end if
       end do
    end do

    if(.not.present(vr)) deallocate(ivr)
    if(.not.present(vc)) deallocate(ivc)

    return
  end subroutine toeplitz

  subroutine heat(a, k)

    implicit none
    real(real64) :: a(:,:)
    real(real64), optional :: k

    real(real64) :: h, pi, c, d, ik
    real(real64), allocatable :: t(:), s(:), r(:)
    integer      :: m, n, i

    m = size(a,1)
    n = size(a,2)
    if(m.ne.n) then
       write(*,'("Heat must be a square matrix.")')
       return
    end if


    if(mod(n, 2) .ne. 0) then
       write(*,'("Heat: dimension must be even.")')
       return
    end if

    ik = 1.d0
    if(present(k)) ik = k

    pi = 4.d0*atan(1.d0)

    h = 1.d0/real(n,real64)

    t = (/(h/2.d0+i*h, i=0,n-1)/)
    c = h/(2.d0*real(ik,real64)*sqrt(pi))
    d = 1.d0/(4*(ik**2))

    ! compute the matrix A
    allocate(s(n), r(n))
    s = 0.d0
    r = 0.d0

    ! [k[i] = c*t[i]^(-1.5)*exp(-d/t[i]) for i in 1:m]
    do i=1,n
       s(i) = c*(t(i)**(-1.5))*exp(-d/t(i))
    end do
    r(1) = s(1)

    call toeplitz(a, s, r)
    ! m = length(t); k = zeros(T, m)
    ! [k[i] = c*t[i]^(-1.5)*exp(-d/t[i]) for i in 1:m]
    ! r = zeros(T, m); r[1] = k[1]
    ! A = toeplitz(T, k, r)
    return
  end subroutine heat


  subroutine phillips(a)

    implicit none
    real(real64) :: a(:,:)

    real(real64) :: h, pi, d, ik
    real(real64), allocatable :: c(:), r1(:)
    integer      :: m, n, i, n4

    m = size(a,1)
    n = size(a,2)
    if(m.ne.n) then
       write(*,'("Phillips must be a square matrix.")')
       return
    end if


    if(mod(n, 4) .ne. 0) then
       write(*,'("Phillips: dimension must be a multiple of 4.")')
       return
    end if

    pi = 4.d0*atan(1.d0)

    h = 12.d0/real(n)
    n4 = n/4

    allocate(r1(n), c(n4+2))
    r1 = 0.d0

    ! c = cos.(T[-1:n4; ] * 4 * π/n)
    c = (/(cos(real(i-2)*4.d0*pi/real(n)), i=1,n4+2)/)

    do i=1,n4
       ! r1[i] = h + 9 / (h * π^2) * (2 * c[i + 1] - c[i] - c[i + 2])
       r1(i) = h+9.d0/(h*pi**2) * (2.d0*c(i+1)-c(i)-c(i+2))
    end do

    ! r1[n4 + 1] = h / 2 + 9 / (h * π^2) * (cos(4 * π / n) - 1)
    r1(n4+1) = h/2.d0 + 9.d0/(h*pi**2) * (cos(4.d0*pi/real(n))-1)

    call toeplitz(A, r1, r1)

    return
  end subroutine phillips

end module utils_mod


