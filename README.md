# Truncated QR factorization with pivoting in mixed precision

This code package is provided for the sole purpose of reproducing
the experimental results in the paper entitled "Truncated QR
factorization with pivoting in mixed precision" by Alfredo Buttari,
Theo Mary and André Pacteau.

The package has two parts described below.

## Julia

This part contains a Julia code that is used to assess the validity of
the theoretical error bound presented in the article. It allows to
reproduce the experimental results presented in section 5.1 of the
article. The code is provided in the form of a Pluto notebook; once
you have started the Julia REPL, you can type the following commands

```
using Pluto
Pluto.run()
```

to launch the Pluto notebook interface in your browser which allows
you to load the provided truncated_multiprec_qr.jl file. This file is
self-documented.


## Fortran

This part contains the optimized Fortran code to reproduce the
performance results presented in section 5.2 of the article. These
results were obtained on an AMD Zen3 EPYC 7763 processor, using the
BLAS and LAPACK routines in the Intel MKL 2020 package and the GNU
gfortran 11.2 Fortran compiler. Results may differ on other platforms
and/or using different BLAS/LAPACK libraries and a different compiler.

### Content

The package contains the following files:

- cli_mod.f90: command-line parsing.
- CMakeLists.txt: CMake configuration file
- dgeqp3_truncated.F: this file contains a slightly adapted version of
  the LAPACK dgeqp3 routine, for double precision computations. It
  takes some additional arguments such as the block size NB, two
  thresholds for truncating the factorization. The first (EPSTR)
  corresponds to the low-rank approximation threshold; the second
  (EPSSW) corresponds to the threshlod that defines the switching from
  double to single precision. Furthermore, it also takes a MAXRANK
  argument that fixes the maximum number of elimination steps that
  have to be performed. On output, it returns the step at which the
  factorization was interrupted in RK and in the UPD argument it
  indicates whether the trailing submatrix is fully updated or not.
- dlagge.f: this routine is simply copied from the LAPACK testing
  suite and is used to generate the randsvd matrix.
- dlaqps_truncated.F: this is used in combination with
  dgeqp3_truncated.F and is a slight adaptation of the lapack dlaqps.F
  file.
- dsgeqp3_truncated.F: this implements the mixed precision
  factorization in double-single precisions.
- ilapmt.f: adapted from the LAPACK dlapmt.f file. It applies a
  permutation to an array of integers.
- laqps_rnd_mod.F90: this file contains double, single and
  mixed-precision implementation of the truncated QR with randomized
  pivoting.
- main.f90: this is the main driver to run experiments.
- partial_dlarfb.f: this is a slight modification of the LAPACK dlarfb
  routine which updates only the topmost rows of the trailing
  submatrix in order to spare some computations when the factorization
  is truncated. Computations are done in double precision arithmetic.
- partial_slarfb.f: same as partial_dlarfb but in single precision.
- sgeqp3_truncated.F: same as dgeqp3_truncated but in single precision.
- slaqps_truncated.F: same as slaqps_truncated but in single precision.
- utils_mod.f90: various utility routines for generating matrices and
  for error checking.


### Building

The code can be installed using the CMake building tool. We advise
building the code in a separate directory. For example, assuming that
we are in the topmost directory of this package, one could do

```
mkdir build
cd build
cmake ../fortran
```

to build the code. CMake should automatically detect the C and Fortran
compilers and the BLAS/LAPACK if they are installed. To guide CMake
into detecting the proper BLAS/LAPACK libraries, please refer to the
FindBLAS documentation
[here](https://cmake.org/cmake/help/latest/module/FindBLAS.html).
Please make sure a **sequantial** BLAS/LAPACK library is used to
conform to the setting use for the experiments in the article.

### Running experiments

Once the package is built, a executable file named main can be run to
perform experiments. As an example, a test can be launched like this

```
./main -b 128 -p 4 -m 8192 -n 8192 -mat phillips -eps 1e-6
```

where

- -b 128: indicates the block size for block algorithms; this also
  defines the size of the sample in the algorithms with randomized
  pivoting (see below). For the experiments in the article a value of
  128 was chosen which seems to deliver best performance when the MKL
  BLAS/LAPACK library is used.
- -p 4: is the oversampling value. The row-size of the sample is,
  therefore, b+p
- -m 8192: the number of rows in the matrix
- -n 8192: the number of columns in the matrix
- -mat phillips: the type of matrix
- -eps 1e-6: the low-rank approximation threshold. This can be a
  comma-separated list for running tests using multiple thresholds.

Additional parameters are available depending on the type of matrix.

- for randsvd: the conditioning can be specified through the -k
  parameter; the default is -k 1e16
- for gravity: the t integration interval is fixed to [0, 1], while
  the s integration interval [a, b] can be specified by the user
  through the -aa and -bb parameters, respectively.  The parameter -dd
  indicates the depth at which the magnetic deposit is located. The
  dafaults are -aa 0.e0, -bb 1.e0 and -dd 0.15e0. Nevertheless, for
  the experiments in the article -dd 0.02 was used because it results
  in a slower decay of the singular values.

When executed, the above command produces the following output:

```
|--------------|------|------|----------|-------------|------|------|---------|------------|----------|----------|
| Matrix       |    M |    N |      eps |       test  | rkd  | rks  | time    |      err   |   dflops |   sflops |
|--------------|------|------|----------|-------------|------|------|---------|------------|----------|----------|
|     phillips | 8192 | 8192 |  0.0E+00 | double_full | 8192 |    0 |  79.687 |  3.459E-15 | 736570.9 |      0.0 |
|     phillips | 8192 | 8192 |  0.0E+00 | single_full | 8192 |    0 |  58.217 |  4.207E-06 |      0.0 | 733577.9 |
|--------------|------|------|----------|-------------|------|------|---------|------------|----------|----------|
|     phillips | 8192 | 8192 |  1.0E-06 |  double_piv |  449 |    0 |  12.453 |  9.912E-07 | 104715.5 |      0.0 |
|     phillips | 8192 | 8192 |  1.0E-06 |   mixed_piv |    7 |  442 |  11.371 |  1.005E-06 |   1878.1 | 112165.6 |
|     phillips | 8192 | 8192 |  1.0E-06 |  single_piv |  472 |    0 |  10.955 |  4.318E-06 |      0.0 | 119541.7 |
|     phillips | 8192 | 8192 |  1.0E-06 |  double_rnd |  509 |    0 |   7.277 |  7.759E-07 | 134667.3 |      0.0 |
|     phillips | 8192 | 8192 |  1.0E-06 |   mixed_rnd |   11 |  508 |   3.557 |  7.273E-07 |  20997.7 | 116255.0 |
|     phillips | 8192 | 8192 |  1.0E-06 |   single_rn |  511 |    0 |   2.043 |  1.432E-06 |      0.0 | 134736.0 |
|--------------|------|------|----------|-------------|------|------|---------|------------|----------|----------|

```

where
- column 1: is the type of matrix
- column 2: the number of rows in the matrix
- column 3: the number of columns in the matrix
- column 4: the used low-rank threshold
- column 5: the used algorithm.
  - double_full: the full factorization run entirely in double
    precision
  - single_full: the full factorization run entirely in double
    precision
  - double_piv: full double-precision code with Businger-Golub
    pivoting
  - mixed_piv: mixed-precision code with Businger-Golub
    pivoting
  - single_piv: full single-precision code with Businger-Golub
    pivoting
  - double_rnd: full double-precision code with randomized
    pivoting
  - mixed_rnd: mixed-precision code with randomized
    pivoting
  - single_rnd: full single-precision code with randomized
    pivoting
- rkd: the number of householder transformations computed in double
  precision
- rks: the number of householder transformations computed in single
  precision
- time: the execution time
- err: the measured error, i.e. norm(A-QR)/norm(A)
- dflops: an estimate of the number of floating-point operations done
  in double precision
- sflops: an estimate of the number of floating-point operations done
  in single precision


The experiment results in section 5.2, Figure 2 can be obtained
executing the following shell command

```bash
for mat in "phillips" "heat" "randsvd" "kahan" "gravity -dd 0.02"; do
   ./main -b 128 -p 4 -m 8192 -n 8192 -mat $mat -eps 1e-14,1e-12,1e-10,1e-8,1e-6;
done
```
  
  
    
    
    
